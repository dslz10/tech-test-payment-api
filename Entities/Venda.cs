using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        
        public int numeroPedido { get; set; }
        public string status { get; set; }
        public string vendedor { get; set; }
        public List<Item> itens  = new List<Item>();

        public void adicionarItem(Item itemRecebido){
            itens.Add(itemRecebido);
        }
    }
}