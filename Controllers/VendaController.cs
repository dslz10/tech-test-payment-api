using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers;

[ApiController]
[Route("[controller]")]
    public class VendaController : ControllerBase
    {
        List<Venda> vendas  = new List<Venda>();


        [HttpGet(Name = "RegistrarVenda/{NumeroPedido}{Vendedor}{itemVendido}")]
		public IActionResult ObterPorId(int NumeroPedido, string Vendedor, string itemVendido)
		{
            Venda v = new Venda();
		    v.numeroPedido=NumeroPedido;
            v.vendedor=Vendedor;
            
			v.status = "Aguardando Pagamento";
			// Validar o tipo de retorno. Se não encontrar a tarefa, retornar NotFound,
			if (NumeroPedido == null)
			{
				return NotFound();
			}
			// caso contrário retornar OK com a tarefa encontrada
			return Ok("Venda numero "+v.numeroPedido+" Realizada com sucesso.");
		}


    }
